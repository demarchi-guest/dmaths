Version 4.4.0.0

Mise en ligne le 6 mai 2017

Version LibreOffice/OpenOffice :

    La configuration utilisateur ou un rapport de bug peut être envoyé aux développeurs (Ctrl+Alt+R/Alt+F7).
    Pour MAC :
        le raccourci clavier de la boîte de dialogue du calcul formel devient Cmd+Alt+X,
        correction d'un bug concernant l'utilisation du calcul formel.

Toutes versions :

    Calcul formel : correction d'un bug apparu dans la récupération des variables enregistrées.


Version 4.3.0.0

Mise en ligne le 4 avril 2017

Version LibreOffice/OpenOffice :

    Amélioration de la gestion des carrés scalaires,
    Windows : Prise en compte des chemins contenant un blanc dans runicas.bat,
    Prise en compte de la correction du bug sur FontDescriptor dans LibreOffice X64 (LO >= 5.3),
    Correction du code pour assurer la compatibilité avec LO 5.3.1.2

Pour les deux :

    Correction d'un bug pour l'affichage vecteurs avec des coordonnées en colonne,
    Le module tableaux de variations gère maintenant la police de caractère (nom et taille),
    Calcul formel : amélioration de la reconnaissance des multiplications implicites, de l'écriture des primitives du type ln(abs(x+1)),

Exemples : f(x)=x(ln+1) est maintenant reconnu comme f(x):=x*(ln(x)+1), de même pour les produits de fonctions k=fg, quotient f/g ou les composées h=g(f),

    Une zone de liste contenant la liste des variables et fonctions mémorisées a été ajoutée à la boîte de dialogue du calcul formel.

Merci à Bernard Parisse le développeur de Xcas pour son aide précieuse.

Version 4.2.0.0

Mise en ligne le 19 janvier 2017

Version LibreOffice/OpenOffice :

    Amélioration de la convivialité de la gestion des options,

Version Word :

    Correction d'un bug concernant les flèches des tableaux de variation,
    Options formules encadrées, textes et formules de la même couleur,
    Outil de réglage des formules d'une plage ou d'un document (taille, police, couleur, bordures ...)

Pour les deux :

    Construction de points sur une courbe et de tangente à la courbe,
    Bouton permettant de mémoriser et de rappeler les formules des fonctions représentées,
    Option formules et bordures de la même couleur.

Version 4.1.0.0

Mise en ligne le 2 janvier 2017

Version LibreOffice/OpenOffice :

    Gestion de la couleur dans les formules,
    Amélioration du module de conversion des documents .doc ou .docx en .odt,

Version Word :

    Les boîtes de dialogue sont au format natif Word,

    Pour les deux : corrections de bugs mineurs.


Version 4.0.0.0

Mise en ligne le 23 décembre 2016

    Mise en ligne de Dmaths pour Word,
    Corrections de bugs mineurs dans la construction des figures.


Version 3.8.1.0

Mise en ligne le 2 décembre 2016

    Corrections de bugs dans le module calcul formel.

Version 3.8.0.0

Mise en ligne le 10 novembre 2016

    Calcul formel : implémentation de la résolution numérique des équations,
    Calcul : avec ou sans utiliser la mémorisation des variables,
    Mise à jour du module AHmath3D avec ajout de la construction du cône tronqué en 3D (merci à Alain Haguet http://ahmath3d.free.fr/),
    Harmonisation des polices pour les tableaux de signes et de variations,
    Écriture des arcs de cercles avec le raccourci clavier [Ctrl+Alt+A],
    Contournement d'un bug d'affichage de cdot sous Windows 8 ou 10,
    Écriture améliorée des dérivées : f(x)=x^2 avec Deriver devient f'(x)=2x,
    Figures géométriques : prise en charge des noms de points ou droites indiciés, exemple A_n.


Version 3.7.0.0

Mise en ligne le 18 juin 2016

    Correction de petits bugs signalés sur le calcul formel,
    Implémentation d'un traitement de l'erreur qui donne un message undef,
    Implémentation de la résolution des systèmes,
    Décomposition en éléments simples dans C : gestion du 1/i → -i,
    Gestion des écritures du type cos^2x → cos(x)^2
    Une option permet un affichage du type 54/7≈7,71 à 10^-2 près.


Version 3.5.9.8

Mise en ligne le 29 février 2016

    Harmonisation complète (Figure en 2D ou 3D) des options de tracé (pointillés, largeur des traits, taille et police des caractères, etc ...),
    Amélioration de l'outil de tracé de figure (bouton G bleu),
    Intégration de la dernière version de AHmath3D. Merci à Alain Haguet,
    Correction d'un bug introduit par la dernière version de LibreOffice (< - devenait le symbole d'affectation ←),
    Correction d'un bug sur les nouvelles installations.

Version 3.5.9.6

Mise en ligne le 23 février 2016

    La touche Retour Arrière (Backspace) peut être configurée dans les options de façon à revenir à la police par défaut.

Version 3.5.9.5

Mise en ligne le 16 février 2016

    Correction d'un bug dans Gdmath concernant les additions et soustractions posées,
    Amélioration de la macro liée avec le retour arrière (Backspace).

Version 3.5.9.4

Mise en ligne le 31 janvier 2016

    L'option permettant de définir trois icônes lançant trois applications choisies par l'utilisateur a été améliorée,
    Correction de bugs se produisant avec les installations multi-utilisateurs
    Correction de quelques autotextes.

Version 3.5.9.3

Mise en ligne le 4 janvier 2016

Pour la 3.5.9.0

    Amélioration de la gestion des options (polices mémorisées et homogénéisées),
    Contournement du bug détecté pour la recherche des polices installées avec LO >= 5 64 bits, sous Windows,
    Reprise de quelques autotextes (ensemble de nombres, symbole appartient...),
    Intégration de la dernière version de AhMaths3D (solides de profil, caractéristiques d'une figure type côtés égaux),
    Possibilité de construire par clic de souris des segments ou droite passant par deux points,
    Possibilité de régler les options de tracer depuis MS Word.

Pour la 3.5.9.3

    Les macros MetEnItalique, MetEnSurligné et MetenAtalante ont été modifiées. Le retour de la police de caractères aux attributs par défaut se fait également à l'intérieur d'une ligne,
    La reconnaissance automatique des raccourcis du type al, be etc n'est plus une option par défaut,

Avec LO >= 5.0.4

    Des corrections automatiques pour obtenir les lettres grecques ont été implémentées. Par exemple __a donnera α. Voir Outils → Options d'Autocorrection → Remplacer.

Version 3.5.8.0

Mise en ligne le 13 décembre 2015

    Refonte des autotextes avec élimination de la police OpenSymbol pour une meilleure harmonie.
    Macro permettant le retour aux attributs d'écriture par défaut.
    Reconnaissance automatique des raccourcis claviers (autotextes) pour les lettres grecques : al + F10 donne la formule avec alpha...
    Installateur graphique pour Windows.
    Construction de figures géométriques possible depuis Word >= 2010.

Version 3.5.7.0

Mise en ligne le 22 novembre 2015

    Amélioration de la stabilité après installation (position et thème des barres d'outils conservés),
    Amélioration de la stabilité pour le changement du thème des icônes,
    Correction de bugs : écriture d'une partie entière, construction d'un cercle.
    Prise en charge de l'écriture des double barres.
    Installateur exécutable pour Windows.
    Refonte des autotextes avec élimination de la police OpenSymbol pour une meilleure harmonie.
    Macro permettant le retour aux attributs d'écriture par défaut.

Version 3.5.6.0

Mise en ligne le 30 octobre 2015 pour les membres du Club Dmaths

    Dmahs est opérationnel pour LO >= 5,
    Correction de bugs mineurs,
    Amélioration du module de constructions géométriques en 2D et 3D (bouton G bleu).

Version 3.5.5.0

Mise en ligne le 30 octobre 2015

    Tous les objets (Formules, Gdmath et AHmath3D) peuvent s'intégrer directement dans un document Draw ou Impress,
    Avec le module de construction géométrique on peut maintenant ajouter un ou plusieurs points, reliés ou non, avec un clic de souris,
    Mise à niveau du fichier Math Guide vers la version 4.0 de LO,
    Le module Gdmath permet de créer un repère en 3D.

Version 3.5.4.0

Mise en ligne le 29 novembre 2014

    amélioration du lancement du module de construction géométrique de Dmaths (icône G bleue),
    intégration d'objets AHmath3D dans ce même module de construction,
    traduction et correction de bugs dus à des problèmes de compatibilité entre les différentes versions de LO (Milifred, Grapheur externe, Module PlotBox Histogramme),

Version 3.5.3.0

Mise en ligne le 14 novembre 2014

    intégration de la dernière version de Ahmath3D,
    Gdmath : Tracer de segments et configurations de Thalès (Alain Haguet),
    Gdmath : correction de bugs (Didier Dorange-Pattoret et Gisbert Friege),
    Dmaths : correction d'un bug portant sur le lien grande parenthèses et symbole intersection, union.

Version 3.5.2.5

Mise en ligne le 30 mars 2014

    correction de bugs lors de l'écriture des fractions dans les vecteurs.
    amélioration du module d'importation des documents au format Latex.

Version 3.5.2.4

Mise en ligne le 15 mars 2014

    correction d'un bug dans Gdmath (écriture des opérations) (Michael Büssow),
    correction d'un bug dans Gdmath (rapporteur et double graduation) (Michael Büssow),
    amélioration pour l'insertion des formules et des dessins dans les documents impress (Didier Dorange-Pattoret).

Version 3.5.2.3

Mise en ligne le 23 février 2014

    correction d'un bug dans les macros graphiques (Michael Büssow),
    correction d'un bug lors de l'écriture des déterminants,
    correction d'un bug et d'un chemin dans le fichier Install Atalante
    amélioration du contournement du bug d'affichage des angles et des vecteurs sous Linux

Version 3.5.2.1

Mise en ligne le 26 janvier 2014

    amélioration des traductions et des autotextes (réalisé par Hélio Guilherme).

Version 3.5.2.0

Mise en ligne le 12 janvier 2014

    mise à jour de Ahmath3D,
    correction d'un bug lié à un confusion entre le rond de la composition et celui des degrés.
    correction d'un bug lorsque LO/AOo est relancé suite à un changement d'options.

Version 3.5.1.9

Mise en ligne le 6 janvier 2014

    réunification des versions LibreOffice et AOOo en une seule

Version 3.5.1.8

Mise en ligne le 5 janvier 2014

    encadrement du texte en mode natif (pour LibO >= 4.2),
    amélioration des boîtes de dialogue bleues et des macros associées,
    introduction de la notation partie entière et des intervalles d'entiers,
    correction d'un bug provoquant un plantage en lançait la boîte de dialogue Dmaths Options en anglais.

Version 3.5.1.7

Mise en ligne le 9 decembre 2013

    Correction d'un bug dans le traceur de courbes qui empêche l'affichage des vecteurs du repère.
    Correction d'un bug dans l'affichage du nom de l'angle, dans la couleur des cercles (Gdmath).
    Correction d'un bug dans les soustractions (Gdmath).
    Amélioration de l'alignement pour les systèmes.
    Correction d'un bug sur le traceur de courbes (cas des valeurs interdites).
    Correction d'un bug concernant la couleur des textes encadrés.
    Traduction en Portugais (Hélio Guilherme).
    Correction et traduction en allemand (Gisbert Friege).

Version 3.5.1.5

Mise en ligne le 24 novembre 2013

    Mise en ligne d'une version fonctionnant sous AOOo4.0.1
    Correction d'un bug sous LibreOffice pour l'édition des additions et multiplications posées.

Version 3.5.1.1

Mise en ligne le 15 mai 2013

    Correction d'un bug (répétition de la commande alignc) pour le centre des termes des fractions.

Version 3.5.1.0

Mise en ligne le 13 mai 2013

    Mise en conformité avec LO 4 : couleur des lignes tracées,
    Correction d'un problème de centrage des termes dans les fractions comportant des majuscules droites,
    Correction d'un bug lors de l'écriture d'une formule commençant par - en début de page,
    Gestion des lettres grecques dans les fractions,
    L'écriture des angles ne traite plus l'option lettres scalaires,
    Amélioration du tracer des courbes.

Version 3.5.0.9

Mise en ligne le 10 mars 2013

    Amélioration du fonctionnement des macros tableaux de variations et de signes

Version 3.5.0.6

Mise en ligne le 6 mars 2013

    Amélioration de la gestion de i, e, j,
    Correction d'un bug sur la macro Selec,
    Correction d'un bug sur les doubles barres dans les tableaux de variations,
    Amélioration et correction d'un bug du module Gdmath,
    Meilleure compatibilité avec LO 4.0

Version 3.4.9.8

Mise en ligne le 13 janvier 2013

    Amélioration de l’outil de redimensionnement des formules
    Amélioration de l’outil de conversions des formules en images,
    Implémentation de l’outil import des documents au format .docx ou .doc
    Retour au code source de la dernière formule écrite si curseur placé juste après,
    Utilisation possible des parenthèses comme séparateur : (x-1)/(x+1) donnera la même fraction que {x-1}/{x+1},
    Gestion des règles typographiques pour les vecteurs : AB en caractère droit, u en italique,
    Message d'avertissement pour les sauvegardes avec synchronisation,
    Valeur absolue, norme, divise s'écrivent avec le caractère |, factoriel ! est géré par défaut,
    Intégration de la dernière version de AHmath3D

Version 3.4.9

Mise en ligne le 30 septembre 2012.

    Intégration de la dernière version de AHmath3D,
    Correction de bugs :
    Introduction de l'écriture en base 2 et en base 10,
    Compatibilité avec OOo 3.4 et LO 3.5

Version 3.4.2

    Intégration de AHmath3D,
    Correction de bugs (traceurs de courbes, boîte de dialogue polygones de Gdmath, redimensionnement des figures) …
    Intégration du presse papier dans le constructeur de courbe,
    Transformation des formules et figures en images,
    Intégration de l'alignement automatique des formules.

Version 3.4

    Le traceur de courbe a été amélioré. Il gère maintenant les valeurs interdites. Les options de tracer peuvent être sauvegardées.
    Une nouvelle galerie contient des objets vectoriels (vecteurs, arcs géométriques et orientés).
    Le traceur de figures géométriques a été beaucoup amélioré : plus ergonomique, plus complet (cercles …), il permet également d'insérer des Objets Gdmath.
    Plusieurs bugs ont été corrigés, des fonctionnalités améliorées comme la reconnaissance des lettres minuscules comme scalaires (option).

Version 3.3

    Adaptation pour OpenOffice.org 3.1 dont internationalisation des boîtes de dialogues.
    Le traceur de courbe a été amélioré. Les paramètres des courbes sont mémorisés dans le dessin. Au lancement de la macro, la courbe est chargée dans la boîte de dialogues.
    Amélioration du constructeur de figures géométriques, mise à jour de Gdmath et OOTep.
    Meilleure ergonomie des menus.
    Arbres de probabilités avec A . Sur-lignage en utilisant la combinaison CTRL+MAJ+F3.
    Correction de quelques bugs.

Version 3.2

    Adaptation pour OpenOffice.org 3.
    L'installation devient indépendante de la plateforme (Windows, Linux ou Mac).
    Tableaux de variations avec dérivée seconde possible.
    Nouvelle fonctionnalité « écriture des isotopes ».
    Ajout de la galerie Dsciences.

Version 3.1

Mise en ligne le 10 mars 2007.

    Installation Click & Play, détection auto. des nouvelles versions et mises à jours en ligne
    Graphismes et paramètres de la barre Dmaths améliorés
    Ajout d'addons :
        OOoTep pour Tracenpoche de Sésamath
        Import/export LaTeX et vers Spip
        Macros GDmaths
        Traceur d'histogrammes et de diagrammes en boites
        fitoo pour la corrélation linéaire
    Meilleure insertion des formules
    Nouveaux raccourcis claviers
    Police curviligne
    Outils pour mises en page conformes

Version 3.0

Mise en ligne le 18 mars 2006.

    Adaptation pour OOo 2.0.
    Réécriture complète du processus d'installation.
    De nombreuses parties du code ont été récrites.
    Une option permet d'adapter automatiquement la taille des formules à celle du texte courant.
    Possibilité de concaténer des formules adjacentes.

Version 2.80

Mise en ligne le 27 novembre 2005.

    Edition de tableaux de variations et de signes.
    Refonte du module autotexte.
    Correction de bugs signalés.
    Nouvelle version du milifred de Frédéric Diaz permettant de créer un papier millimétré.

Version 2.71

    Correction du problème d'affichage ou d'impression de certains caractères lors de l'utilisation des autotextes.

Les nouveautés de la version 2.70:

    Dmaths peut être installé en mode réseau ou multi-utilisateurs.
    Le produit s'écrit avec la touche * du pavé numérique, ou du clavier.
    De nouvelles options permettent de choisir le mode d'écriture des produits de fractions, d'utiliser le point d'exclamation ! pour écrire les valeurs absolues ou les factorielles.
    Un module de sauvegarde est accessible par le bouton B jaune de la barre d'icônes, une option permet de rappeler le délai écoulé depuis la dernière sauvegarde.

Attention : Ce module a été testé, mais nous vous recommandons de conserver par copier-coller des sauvegardes récentes !

    Un bouton permettant d'écrire plus facilement le conjugué d'un nombre complexe est disponible dans la boîte de dialogue Macros bleues.
    Intégration du milifred de Frédéric Diaz permettant de créer un papier millimétré.
    Ajout de nombreux autotextes: coefficients binomiaux, les ensembles ℝ+∗, ℚ+∗, ℤ+∗ et analogues, réunion et intersection d'une famille d'ensembles ...
    Correction de bugs.
    Intégration du module BatchConv qui pemet d'exporter des fichiers ou tout un répertoire dans le format de son choix en particulier pdf.

Version 2.60

Mise en ligne le 12 décembre 2004.

    Possibilité d'encadrer du texte ou une formule,
    Réglage automatique de la taille des parenthèses en fonction de la formule,
    Macro MetEnItalic et nouveaux autotextes pour se rapprocher des règles typographiques officielles,
    Prise en charge des indices et lettres grecques pour les vecteurs, angles, mesures algébriques,
    Ecriture en caractère droit du e de l'expoentielle, des i et j des complexes,
    Nouvelles figures incluses dans la galerie: 119 au total,
    Installation pour Mac Os X,
    Howto Formule du projet francophone OpenOffice.org fourni avec le mode d'emploi,
    Mémo OOo-Dmaths sur 2 pages, avec un format carte postale à imprimer,
    Encore moins de bugs et une meilleure ergonomie, grâce à la contribution de plusieurs utilisateurs.

Version 2.5

Mise en ligne le 7 novembre 2004.

    Le bug provoqué après l'édition des formules est corrigé: le mode automatique ne sautera plus.
    La gestion des niveaux de parenthèses multiples est assurée correctement.
    Les formules peuvent être éditées en mode normal ou espacé.
    La gestion des intervalles ouverts est simplifiée.
    Les autotextes sont entièrement réécrits, notamment pour améliorer la prise en charge des lettres grecques et pour des formules plus conformes aux règles typographiques.
    Ecriture possible des limites en mode superposé (x tend vers 0, x>0).
    Correction de Bugs rencontrés lors de l'installation (Voir FAQ).
    Création de 2 galeries avec 72 figures géométriques fournies.
    Prise en charge de la virgule sur le pavé numérique pour la version 1.1.3 de OOo.

Version 2.4

    intégration de trois nouveaux addons:
        MultiSave de StarXpert, permet de sauvegarder simultanément sous plusieurs formats (.sxw, .doc, .pdf),
        OOoVirgule de Rémy Peyronnet (module d'installation de Laurent Godard) permet d'obtenir sous windows la virgule sur le pavé numérique,
        PincOO de Diogène Moulron permet de copier et de coller des styles.
    correction de bugs dans Graphplotter,
    révision du module d'installation,
    ajout d'autotextes : repère en dimension 3, majuscules accentuées, exemples ...

Version 2.3

    possibilité d'insérer des tableaux prédéfinis,
    écriture des équations dans les tableaux,
    macro pour redéfinir la taille et la police d'une équation ou de toutes les équations sélectionnées ou écrites dans un document,
    option sur la taille des intégrales et mode texte,
    mémorisation des options définies pour une modification plus rapide,
    écriture d'un produit, d'une dérivée pointée ou double pointée,
    double choix pour l'écriture des systèmes,
    vecteurs à coordonnées en colonnes,
    prise en compte de certaines nouvelles API de la future version 2.0 (installation facilitée),
    installation séparée des autotextes selon la langue. Permet d'éviter les doublons,
    intégration du GraphPlotter de Andy Lewis.

Version 2.2

    prise en compte des fonctionnalités de OO1.1, avec insertion automatique optionnelle des formules,
    version espagnole,
    module traceur de figures géométriques.

Version 2.13

    modification possible des courbes sans tout retaper,
    respect des règles typographiques usuelles,
    module options du logiciel,
    en danois et slovaque.

Version 2.12

    correction du bug sur les flèches de vecteurs,
    amélioration du module traceur de courbes avec automatisation si besoin de la fenêtre de tracé et calcul de f(x),
    module plotteur, courbes paramétriques et polaires,
    module traceur de grille, de points,
    module statistiques avec calcul des valeurs caractéristiques d'une série, sauvegarde et tracer de diagrammes en boîtes,
    bouton de lancement de dia (si installé),
    amélioration de l'autotexte...
    installation automatisée ...
    correction des bugs sous linux à la fermeture de certaines boîtes de dialogues.
    en anglais et allemand

