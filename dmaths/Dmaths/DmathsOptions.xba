<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="DmathsOptions" script:language="StarBasic">&apos;************************************************
&apos;Copyright (C) 2008-2017 Didier Dorange-Pattoret
&apos;38, chemin de l&apos;Abbaye  
&apos;74940 Annecy le Vieux
&apos;France
&apos;ddorange@dmaths.com
&apos;
&apos;
&apos;
&apos;This library is free software; you can redistribute it and/or
&apos;modify it under the terms of the GNU General Public Licence (GPL)
&apos;as published by the Free Software Foundation; either
&apos;version 2.1 of the License, or (at your option) any later version.

&apos;This library is distributed in the hope that it will be useful,
&apos;but WITHOUT ANY WARRANTY; without even the implied warranty of
&apos;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
&apos;General Public License for more details.

&apos;You should have received a copy of the GNU General Public Licence (GPL)
&apos;along with this library; if not, write to the Free Software
&apos;Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
&apos;************************************************

Rem options générales
Global iLang as Integer
Global iNumVersionDmathsCourante as Integer
Global iNumVersionDmathsPlusCourante as Integer
Global bItalic as Boolean
Global bEdroit as Boolean
Global bIdroit as Boolean
Global bOptionMajusculesItalic as Boolean
Global bLettresAreScalaires as Boolean
Global sLettresScalaires as String
Global bFormulesGras as Boolean
Global iTailleFormules as Integer
Global sPoliceFormules as String
Global bTailleFormCarac as Boolean
Global bPoliceFormCarac as Boolean
Global iMargesFormules as Integer
Global bConcateneFormules as Boolean
Global sBorneInfIntegrales as String
Global sBorneSupIntegrales as String
Global bTextMode as Boolean
Global bCoordColonne as Boolean
Global bSystemesAlignes as Boolean
Global bMatricesRondes as Boolean
Global bFormulesEspacees as Boolean
Global bFormulesEncadrees as Boolean
Global bGrandesParentheses as Boolean
Global bFractions as Boolean
Global bXpointaulieux as Boolean
Global sCouleurFormules as String
Global iCouleurFormules as Long
Global sCaracteresAColorier as String
Global bCouleurTexteFormules as Boolean
Global bCouleurCadresFormules as Boolean
Global bUsingDispatcher as Boolean
Global bConvertLatin2Grec as Boolean
Global iDelaiEntreSauvegardes as Integer
Global sThemeIcons as String
Global sPositionDesBarresdOutilsMemorisee as String
Global iModeAffichageIcones as Integer
Global sCheminSoftware1 as String
Global sCheminSoftware2 as String
Global sCheminSoftware3 as String
Global sNomPoliceCursive as String
Global bIsWin8or10 as Boolean
Global bUseWordAPI As Boolean
Global sCheminDmathsUtilisateur as String
Global sCheminIcas as String
Global sCheminIcasBatch as String

Rem options tracé
Global sOptionsTraceCourbe As String
Rem options fenetres
Global ViewX, ViewY, ViewZ as Long
Rem options tableau
Global iNbreLignes as Integer
Global iNbreColonnes as Integer
Global bTitre as Boolean
Global bBordure as Boolean
Global bCentreHorizontal as Boolean
Global bCentreVertical as Boolean
Rem options backup
Global sDateSauvegarde as String
Global sRepertoireSourceDefaut as String
Global sRepertoireBackupDefaut as String
Global iNombreRepertoiresTraites as Integer

Sub LoadOptionsDmaths
	Dim sCheminDmOptions, sparamName, sparamValue, sOneLine  as String
	Dim x As Long 
	
	rem traitement des erreurs
	On Error GoTo TraitementErreurLoadOptionsDmaths
	
	sCheminDmOptions = ConvertToURL(GetRepertoirePath(&quot;user&quot;) &amp; GetPathSeparator &amp; &quot;Dmaths&quot; &amp; GetPathSeparator) 
	If Not FileExists(sCheminDmOptions) Then MkDir sCheminDmOptions
	sCheminDmOptions = sCheminDmOptions &amp; &quot;dmoptions.txt&quot;
	If Not FileExists(sCheminDmOptions) Then 			
		FileCopy fCheminDmaths &amp; &quot;Options/dmoptions.txt&quot; , sCheminDmOptions	
	Endif
		
	iNumber = Freefile
	Open sCheminDmOptions For Input As iNumber
		Do While not Eof(iNumber)
		Line Input #iNumber, sOneLine
		x = Instr(sOneLine,&quot;=&quot;)
			if x &gt; 0  then
      			paramName =  Trim( Left(sOneLine, x-1) )
      			paramValue = Trim( Mid(sOneLine, x+1) )
      		&apos;	Print paramName,  paramValue				
      			Select Case paramName
					Case &quot;iLang&quot;	
						iLang = CInt(paramValue)				 
       				Case &quot;iNumVersionDmathsCourante&quot;
       					iNumVersionDmathsCourante = CInt(paramValue)
       				Case &quot;iNumVersionDmathsPlusCourante&quot;
       					iNumVersionDmathsPlusCourante = CInt(paramValue)
       				Case &quot;bItalic&quot;
       					bItalic = CBool(paramValue)
       				Case &quot;bEdroit&quot;
       					bEdroit = CBool(paramValue)
       				Case &quot;bIdroit&quot;
       					bIdroit = CBool(paramValue)
       				Case &quot;bOptionMajusculesItalic&quot;
       					bOptionMajusculesItalic = CBool(paramValue)
       				Case &quot;bLettresAreScalaires&quot;
       					bLettresAreScalaires = CBool(paramValue)
       				Case &quot;sLettresScalaires&quot;
       					sLettresScalaires = CStr(paramValue)
       				Case &quot;bFormulesGras&quot;
       					bFormulesGras = CBool(paramValue)
       				Case &quot;iTailleFormules&quot;
       					iTailleFormules = CInt(paramValue)
       				Case &quot;sPoliceFormules&quot;
       					sPoliceFormules = CStr(paramValue)       				
       				Case &quot;bTailleFormCarac&quot; 
       					bTailleFormCarac = CBool(paramValue)
       				Case &quot;bPoliceFormCarac&quot;
       					bPoliceFormCarac = CBool(paramValue)
  					Case &quot;iMargesFormules&quot;
  						iMargesFormules = CInt(paramValue) 
  					Case &quot;bConcateneFormules&quot;
  						bConcateneFormules = CBool(paramValue)
  					Case &quot;sBorneInfIntegrales&quot;
  						sBorneInfIntegrales = CStr(paramValue)
  					Case &quot;sBorneSupIntegrales&quot;
  						sBorneSupIntegrales = CStr(paramValue)
  					Case &quot;bTextMode&quot;
  						bTextMode = CBool(paramValue)
  					Case &quot;bCoordColonne&quot;
  						bCoordColonne = CBool(paramValue)
  					Case &quot;bSystemesAlignes&quot;
  						bSystemesAlignes = CBool(paramValue)
  					Case &quot;bMatricesRondes&quot;
  						bMatricesRondes = CBool(paramValue)
  					Case &quot;bFormulesEspacees&quot;
  						bFormulesEspacees = CBool(paramValue)
  					Case &quot;bFormulesEncadrees&quot;
  						bFormulesEncadrees = CBool(paramValue)
					Case &quot;bGrandesParentheses&quot;
						bGrandesParentheses = CBool(paramValue)
					Case &quot;bFractions&quot;
						bFractions = CBool(paramValue)
					Case &quot;bXpointaulieux&quot;
						bXpointaulieux = CBool(paramValue)
					Case &quot;sCouleurFormules&quot;
						sCouleurFormules = CStr(paramValue)
					Case &quot;iCouleurFormules&quot;
						iCouleurFormules = CLng(paramValue)	
					Case &quot;sCaracteresAColorier&quot;
						sCaracteresAColorier = CStr(paramValue)
					Case &quot;bCouleurTexteFormules&quot;
						bCouleurTexteFormules = CBool(paramValue)		
					Case &quot;bCouleurCadresFormules&quot;
						bCouleurCadresFormules = CBool(paramValue)
					Case &quot;bUsingDispatcher&quot;
						bUsingDispatcher = CBool(paramValue)
					Case &quot;bConvertLatin2Grec&quot;
						bConvertLatin2Grec = CBool(paramValue)					
					Case &quot;iDelaiEntreSauvegardes&quot;
						iDelaiEntreSauvegardes = CInt(paramValue)
					Case &quot;sThemeIcons&quot;
						sThemeIcons = CStr(paramValue)
					Case &quot;sPositionDesBarresdOutilsMemorisee&quot;
						sPositionDesBarresdOutilsMemorisee = CStr(paramValue)
					Case &quot;iModeAffichageIcones&quot; 
						iModeAffichageIcones = CInt(paramValue)
					Case &quot;sCheminSoftware1&quot;
						sCheminSoftware1 = CStr(paramValue)
				    Case &quot;sCheminSoftware2&quot;
						sCheminSoftware2 = CStr(paramValue)					
					Case &quot;sCheminSoftware3&quot;
						sCheminSoftware3 = CStr(paramValue)	
					Case &quot;sNomPoliceCursive&quot;
						sNomPoliceCursive = CStr(paramValue)
					Case &quot;bUseWordAPI&quot;
						bUseWordAPI = CBool(paramValue)
					Case &quot;bIsWin8or10&quot;
						bIsWin8or10 = CBool(paramValue)				
					Case &quot;sCheminDmathsUtilisateur&quot;
						sCheminDmathsUtilisateur = CStr(paramValue)
					Case &quot;sCheminIcasBatch&quot;
						sCheminIcasBatch = CStr(paramValue)
					Case &quot;sCheminIcas&quot;
						sCheminIcas = CStr(paramValue)			
				End Select
			Endif
		Loop
		Close #iNumber
		
Rem options de tracer
	sCheminDmOptions = sCheminDmathsUtilisateur &amp; &quot;dmoptionstrace.txt&quot; 
&apos;	sCheminDmOptions = GetRepertoirePath(&quot;user&quot;) &amp; GetPathSeparator &amp; &quot;Dmaths&quot; &amp; GetPathSeparator &amp; &quot;dmoptionstrace.txt&quot; 
	If FileExists(sCheminDmOptions) Then 	
		iNumber = Freefile
		Open sCheminDmOptions For Input As iNumber
			Do While not Eof(iNumber)
			Line Input #iNumber, sOneLine
			x = Instr(sOneLine,&quot;=&quot;)
				if x &gt; 0  then
	      			paramName =  Trim( Left(sOneLine, x-1) )
	      			paramValue = Trim( Mid(sOneLine, x+1) )	      					
	      			Select Case paramName
						Case &quot;sOptionsTraceCourbe&quot;
	       					sOptionsTraceCourbe = CStr(paramValue)	       		
					End Select
				Endif
			Loop
			Close #iNumber	
	Endif
	
Rem options de fenetre d&apos;affichage pour la construction des figures
	sCheminDmOptions = sCheminDmathsUtilisateur &amp;  &quot;dmoptionsfenetre.txt&quot; 
&apos;	sCheminDmOptions = GetRepertoirePath(&quot;user&quot;) &amp; GetPathSeparator &amp; &quot;Dmaths&quot; &amp; GetPathSeparator &amp; &quot;dmoptionsfenetre.txt&quot; 
	If FileExists(sCheminDmOptions) Then 				
		iNumber = Freefile
		Open sCheminDmOptions For Input As iNumber
			Do While not Eof(iNumber)
			Line Input #iNumber, sOneLine
			x = Instr(sOneLine,&quot;=&quot;)
				if x &gt; 0  then
	      			paramName =  Trim( Left(sOneLine, x-1) )
	      			paramValue = Trim( Mid(sOneLine, x+1) )	      					
	      			Select Case paramName
						Case &quot;ViewX&quot;
	       					ViewX = CLng(paramValue)	 
	       				Case &quot;ViewY&quot;
	       					ViewY = CLng(paramValue)	   
	       				Case &quot;ViewZ&quot;
	       					ViewZ = CInt(paramValue)	         		
					End Select
				Endif
			Loop
			Close #iNumber	
	Endif
	
	Rem si le delai entre deux sauvegardes est non nul alors on charge les options de sauvegarde
	If iDelaiEntreSauvegardes &lt;&gt; 0 Then LoadOptionsBackupDmaths
	
	Exit Sub

	
	rem traitement des erreurs
	TraitementErreurLoadOptionsDmaths:
	Print &quot;Erreur dans LoadOptionsDmaths : ligne &quot; &amp; erl &amp;  &quot;   &quot; &amp; Error
	Resume next	
	
End Sub

rem --------------------------------------------------------------------------------------------------------------------------------------------
Sub LoadOptionsTableauDmaths
	sCheminDmOptions = sCheminDmathsUtilisateur &amp;  &quot;dmoptionstableau.txt&quot; 
	If FileExists(sCheminDmOptions) Then 				
		iNumber = Freefile
		Open sCheminDmOptions For Input As iNumber
			Do While not Eof(iNumber)
			Line Input #iNumber, sOneLine
			x = Instr(sOneLine,&quot;=&quot;)
				if x &gt; 0  then
	      			paramName =  Trim( Left(sOneLine, x-1) )
	      			paramValue = Trim( Mid(sOneLine, x+1) )	      					
	      			Select Case paramName
						Case &quot;iNbreLignes&quot;
	       					iNbreLignes = CInt(paramValue) 
	       				Case &quot;iNbreColonnes&quot;
	       					iNbreColonnes = CInt(paramValue)	   
	       				Case &quot;bTitre&quot;
	       					bTitre = CBool(paramValue)
	       				Case &quot;bBordure&quot;
	       					bBordure = CBool(paramValue)	 
	       				Case &quot;bCentreHorizontal&quot;
	       					bCentreHorizontal = CBool(paramValue)	   
	       				Case &quot;bCentreVertical&quot;
	       					bCentreVertical = CBool(paramValue)	         		
					End Select
				Endif
			Loop
			Close #iNumber	
	Endif
End Sub

rem --------------------------------------------------------------------------------------------------------------------------------------------
Sub LoadOptionsBackupDmaths
	sCheminDmOptions = sCheminDmathsUtilisateur &amp;  &quot;dmoptionsbackup.txt&quot; 
	If FileExists(sCheminDmOptions) Then 				
		iNumber = Freefile
		Open sCheminDmOptions For Input As iNumber
			Do While not Eof(iNumber)
			Line Input #iNumber, sOneLine
			x = Instr(sOneLine,&quot;=&quot;)
				if x &gt; 0  then
	      			paramName =  Trim( Left(sOneLine, x-1) )
	      			paramValue = Trim( Mid(sOneLine, x+1) )	      					
	      			Select Case paramName
						Case &quot;sDateSauvegarde&quot;
	       					sDateSauvegarde = CStr(paramValue)
	       				Case &quot;sRepertoireSourceDefaut&quot;
	       					sRepertoireSourceDefaut = CStr(paramValue)	   
	       				Case &quot;sRepertoireBackupDefaut&quot;
	       					sRepertoireBackupDefaut = CStr(paramValue)
	       				Case &quot;iNombreRepertoiresTraites&quot;
	       					iNombreRepertoiresTraites = CInt(paramValue)	       				         		
					End Select
				Endif
			Loop
			Close #iNumber	
	Endif
End Sub




</script:module>